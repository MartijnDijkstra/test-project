package com.martijn;
import java.util.*;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        //Declare test string
        String stringOfWords = "test test test test Lorem2    Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";

        System.out.println("Highest Frequency");
        System.out.println(calculateHighestFrequency(stringOfWords));

        String wordToFrequency = "ipsum";
        System.out.println("Calculate Frequency of word: [" + wordToFrequency+ "]");
        System.out.println(calculateFrequencyForWord(stringOfWords, wordToFrequency));

        int totalN = 5;
        System.out.println("Calculate the " + totalN + " most frequent word(s)");
        System.out.println(calculateMostFrequentNWords(stringOfWords, totalN));
    }

    public static int calculateHighestFrequency(String text) {
        //Put String into Hashmap
        HashMap<String, Integer> validatedWordsHashMap = new HashMap();
        validatedWordsHashMap = cleanStringOfWordsAndConvertToHashMap(text);

        //Loop over hashmap and search for highest number
        int highestNumber = 0;
        for (Map.Entry<String, Integer> entry : validatedWordsHashMap.entrySet()) {
            if(entry.getValue().intValue() > highestNumber){
                highestNumber = entry.getValue().intValue();
            }
        }

        return highestNumber;
    }

    public static int calculateFrequencyForWord(String text, String word) {
        HashMap<String, Integer> validatedWordsHashMap = new HashMap();
        validatedWordsHashMap = cleanStringOfWordsAndConvertToHashMap(text);

        if(validatedWordsHashMap.get(word) == null){
            return 0;
        }

        return validatedWordsHashMap.get(word);
    }

    public static List<String> calculateMostFrequentNWords(String text, int n) {
        HashMap<String, Integer> validatedWordsHashMap = new LinkedHashMap();
        validatedWordsHashMap = cleanStringOfWordsAndConvertToHashMap(text);

        validatedWordsHashMap = sortByValue(validatedWordsHashMap);
        List<Integer> wordCountNumberOrdered = new ArrayList<Integer>(validatedWordsHashMap.values());
        List<String> wordCountWordOrdered = new ArrayList<String>(validatedWordsHashMap.keySet());

        List<String> finalList = new ArrayList<String>();
        int lenghtOfArray = validatedWordsHashMap.size();
        for (int i = 0; i < n; i++) {
            lenghtOfArray--;
            finalList.add("(\""+wordCountWordOrdered.get(lenghtOfArray)+"\", "+wordCountNumberOrdered.get(lenghtOfArray)+")");
        }


        return finalList;
    }

    public static HashMap<String, Integer> cleanStringOfWordsAndConvertToHashMap(String words){
        //Remove spaces / lowercase / split
        String[] arrayOfWords = words.toLowerCase().replaceAll("( +)"," ").trim().split(" ");

        //validate words
        List<String> validatedWords = new ArrayList<>();
        for (String possibleWord : arrayOfWords) {
            if(validateString(possibleWord)) {
                validatedWords.add(possibleWord);
            }
        }

        //Put String into Hashmap
        HashMap<String, Integer> wordCount = new HashMap();
        for (String word : validatedWords) {
            if ( wordCount.containsKey(word) ){
                int count = wordCount.get(word);
                count++;
                wordCount.put(word,count);
            }else{
                wordCount.put(word,1);
            }
        }

        return wordCount;
    }

    public static boolean validateString(String word) {
        //Search in word for Digits if digit is found then return false
        for(char c : word.toCharArray()){
            if(Character.isDigit(c)) {
                return false;
            }
        }
        return true;
    }

    public static HashMap<String, Integer> sortByValue(HashMap<String, Integer> hm)
    {
        // Create a list from elements of HashMap
        List<Map.Entry<String, Integer> > list = new LinkedList<Map.Entry<String, Integer> >(hm.entrySet());

        // Sort the list
        Collections.sort(list, new Comparator<Map.Entry<String, Integer> >() {
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2)
            {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        // put data from sorted list to hashmap
        HashMap<String, Integer> temp = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> aa : list) {
            temp.put(aa.getKey(), aa.getValue());
        }
        return temp;
    }
}
